# frozen_string_literal: true

Ubill.configure do
  #debug_bounding_boxes
  page_size "A4"
  footer.configure do
    add_line do
      append "rechnung GmbH", styles: [:bold]
      append "Mattermoststraße 23"
      append "12345 Demoburg"
      append "info@example.org"
      append "www.example.org"
    end
    add_line do
      append "Geschäftsführer Fried Hof, Holger Manen, Manfred Erike"
    end
    add_line do
      append "HRB 4711"
      append "Amtsgericht Jurahausen"
      append "Steuernummer 123/456/78900"
    end
  end
  header.configure do
    left "rechnung GmbH"
    right do
      append "Mattermoststraße 23"
      append "12345 Demoburg"
      append "info@example.org"
    end
  end
  account.configure do
    recipient "rechnung GmbH"
    iban "DE081547114200"
    bic "ACCOUNTXXX"
  end
end

RSpec.describe Ubill do
  it "has a version number" do
    expect(Ubill::VERSION).not_to be nil
  end

  context "render documents" do
    before do
      @doc = Ubill::Invoice.new do |doc|
        doc.subject("Rechnung R1234567890", "Rechnungsdatum 01.01.2100")
        doc.recipient(
          organisation: "Spielvereinigung Demoburg e.V.",
          name: "Tobi Ornottobi",
          department: "Dept. Purchase",
          street_1: "Highwaytohell 2",
          street_2: "Room 3.12",
          zip: "12345",
          city: "Coruscant",
          country: "Galactic Empire"
        )
        4.times do |idx|
          doc.position(
            name: "Standard Abonnement",
            description: "SpVgg Demoburg -- Platz #{idx+1}\nStandard Abonnement für ein Jahr",
            price_per_unit: "7,50 € / Monat",
            units: "12",
            price: "90,00 €"
          )
        end
        doc.total "180,00 €"
        doc.due "24.12.2100"
        doc.addendum "Dazu kann ich nur noch das hier sagen."
      end
    end

    it "generates a pdf file" do
      path = "tmp/saved.pdf"
      @doc.save_as(path)
      expect(File).to exist(path)
    end

    it "generates pdf to string" do
      path = "tmp/rendered.pdf"
      File.binwrite(path, @doc.render)
      expect(File).to exist(path)
    end
  end
end
