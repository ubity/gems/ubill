class Hash
  def each_with_filter(*filter, &block)
    filter.each do |key|
      if key.is_a?(Array)
        values = key.map do |k|
          self.fetch(k) if has_key?(k)
        end
        values.compact!
        block.call(key, values.join(" "))
      else
        with(key, &block)
      end
    end
  end

  def with(key)
    yield(key, fetch(key)) if has_key?(key)
  end
end
