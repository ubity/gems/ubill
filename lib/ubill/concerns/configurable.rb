module Ubill
  module Concerns
    module Configurable
      def configure(&block)
        self.instance_eval(&block)
        self
      end
    end
  end
end
