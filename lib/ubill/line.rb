module Ubill
  class Line < Array
    def append(item, **options)
      unless self.empty?
        self << {text: " • ", character_spacing: 1.mm}
      end

      # don't know why, but sometimes there appears an additional
      # space after the item. seems to be a certain combination of
      # characters, maybe problems with UTF-8? However, if \0 is
      # appended symptom vanishes.
      options[:text] = "#{item}\0"
      self << options
    end
  end
end
