require_relative "line"

module Ubill
  class Header
    include Ubill::Concerns::Configurable

    attr_accessor :left_text, :right_text

    def initialize
      self.left_text = ""
      self.right_text = Line.new
    end

    def left(value)
      self.left_text = value
    end

    def right(&block)
      right_text.instance_eval(&block)
    end
  end
end
