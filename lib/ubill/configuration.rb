require_relative "header"
require_relative "footer"
require_relative "account"

module Ubill
  def self.configure(&block)
    @config ||= Configuration.new
    @config.instance_eval(&block)
  end

  def self.config
    @config ||= Configuration.new
  end

  class Configuration
    attr_accessor :options

    def initialize
      self.options = {
        page_size: "A4",
        margin: [2.8.cm, 2.8.cm, 3.2.cm, 2.8.cm]
      }
    end

    def page_size(value)
      options[:page_size] = value
    end

    def footer
      @footer ||= Footer.new
    end

    def header
      @header ||= Header.new
    end

    def account
      @account ||= Account.new
    end

    def debug_bounding_boxes
      @debug_bounding_boxes = true
    end

    def debug_bounding_boxes?
      @debug_bounding_boxes || false
    end
  end
end
