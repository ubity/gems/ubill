require_relative "line"

module Ubill
  class Footer
    include Ubill::Concerns::Configurable

    attr_accessor :lines

    def initialize
      self.lines = []
    end

    def add_line(&block)
      line = Line.new
      line.instance_eval(&block)
      self.lines << line
    end
  end
end
