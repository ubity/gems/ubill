require_relative "line"

module Ubill
  class Account
    include Ubill::Concerns::Configurable

    def recipient(value=nil)
      @recipient = value unless value.nil?
      @recipient
    end

    def iban(value=nil)
      @iban = value unless value.nil?
      @iban
    end

    def bic(value=nil)
      @bic = value unless value.nil?
      @bic
    end
  end
end
