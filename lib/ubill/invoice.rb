require "prawn"
require "prawn/table"
require "prawn/measurement_extensions"

require "json"

module Ubill
  class Invoice
    include Prawn::View
    include Ubill::Concerns::Configurable

    COLOR_SECONDARY = "777777"
    EXTERNAL_FONTS_DIR = File.join(__dir__, "fonts")

    def initialize
      super()
      @recipient = {}
      @subject = ""
      @subject_info = ""
      @positions = [["#", "Beschreibung", "Einzelpreis", "Menge", "Gesamt"]]
      @total = ""
      @due = ""
      @addendum = ""

      load_external_font_families

      yield(self) if block_given?
    end

    def recipient(**options)
      @recipient.merge!(options)
    end

    def subject(title, subtitle=nil)
      @subject = title
      @subject_info = subtitle
    end

    def position(name:, description:, price_per_unit:, units:, price:)
      @positions << [
        @positions.size,
        "<b>#{name}</b><font size=\"9\">\n#{description}</font>",
        price_per_unit,
        units,
        price
      ]
    end

    def total(value)
      @total = value
    end

    def due(value)
      @due = value
    end

    def addendum(value)
      @addendum = value
    end


    def config
      Ubill.config
    end

    def document
      @doc ||= Prawn::Document.new(config.options)
    end

    def width
      bounds.top_right[0]
    end

    def height
      bounds.top_left[1]
    end

    def margin_top
      config.options[:margin][0]
    end

    def margin_right
      config.options[:margin][1]
    end

    def margin_bottom
      config.options[:margin][2]
    end

    def margin_left
      config.options[:margin][3]
    end

    def save_as(*args, **options)
      describe
      super
    end

    def render(*args, **options)
      describe
      super
    end

    private
      def load_external_font_families
        Dir.glob(File.join(EXTERNAL_FONTS_DIR, "*")) do |family_dir|
          family = File.basename(family_dir)
          files = {}
          Dir.glob(File.join(family_dir, "*.ttf")) do |file|
            style = File.basename(file, ".*")
            files[style.to_sym] = file
          end
          font_families.update(family => files)
        end
      end

      def describe
        default_leading 0.mm
        default_kerning true

        font "OpenSans"

        describe_header
        move_down 9.mm
        pad 9.mm do
          describe_address
        end
        pad 9.mm do
          describe_subject
        end
        pad 9.mm do
          describe_positions
        end
        pad 2.mm do
          describe_addendum
        end
        pad 2.mm do
          describe_terms
        end
        pad 2.mm do
          describe_payment
        end
        describe_footer
      end

      def describe_header
        repeat(:all) do
          bounding_box([0, height + margin_top], width: width/3, height: margin_top - 1.cm) do
            stroke_bounds if config.debug_bounding_boxes?
            text config.header.left_text, style: :bold, align: :left, valign: :bottom, size: 13, color: COLOR_SECONDARY
          end
        end
        bounding_box([width/3, height + margin_top], width: width/3*2, height: margin_top - 1.cm) do
          stroke_bounds if config.debug_bounding_boxes?
          formatted_text config.header.right_text, align: :right, valign: :bottom, size: 8, color: COLOR_SECONDARY
        end
      end

      def describe_address
        @recipient.each_with_filter(
          :organisation, :name, [:firstname, :lastname], :department, :street_1, :street_2, [:zip, :city], :country
        ) do |key, value|
          move_down 0.3.cm if key.is_a?(Array) && key.eql?([:zip, :city])
          text value, size: 10
        end
      end

      def describe_subject
        text @subject, size: 11, style: :bold
        text @subject_info, size: 8
      end

      def describe_positions
        data = @positions + [[{content: "Gesamtbetrag:", colspan: 4}, @total]]
        table(data,
          width: width,
          header: true,
          cell_style: {inline_format: true, size: 10}
        ) do
          cells.borders = [:bottom]
          cells.border_color = "555555"
          cells.border_width = 0.3
          row(0).border_width = 0.88
          row(0).font_style = :bold
          row(0).size = 11
          row(data.size-1).borders = [:top]
          row(data.size-1).border_width = 0.88
          row(data.size-1).font_style = :bold
          cells.style do |cell|
            if cell.column == 2
              cell.style width: 30.mm, align: :right
            end

            if cell.column == 3
              cell.style width: 20.mm, align: :right
            end

            if cell.column == 4
              cell.style width: 25.mm, align: :right
            end

            if cell.row == data.size-1
              cell.style align: :right
            end
          end
        end
      end

      def describe_addendum
        font_size 10
        text @addendum
      end

      def describe_terms
        font_size 10
        text "Es wird gemäß §19 Abs. 1 Umsatzsteuergesetz keine Umsatzsteuer erhoben. Der Gesamtbetrag der Rechnung ist zahlbar bis zum #{@due} ohne Abzug. Wenn nicht anders angegeben, entspricht das Leistungsdatum dem Rechnungsdatum."
      end

      def describe_payment
        font_size 10
        text "Bitte überweisen Sie den Gesamtbetrag an:"
        move_down 1.mm
        table(
          [
            ["Empfänger:", config.account.recipient],
            ["IBAN:", config.account.iban],
            ["BIC/SWIFT:", config.account.bic],
            ["Verwendungszweck:", @subject]
          ],
          cell_style: {size: 10}
        ) do
          cells.borders = []
          cells.padding = [0, 0, 0.75.mm, 0]
          cells.style do |cell|
            if cell.column == 0
              cell.style padding_left: 12.mm
            else
              cell.style padding_left: 8.mm
            end
          end
        end
      end

      def describe_footer
        repeat(:all) do
          bounding_box([0, -1.cm], width: width, height: margin_bottom - 1.cm) do
            stroke_bounds if config.debug_bounding_boxes?
            config.footer.lines.each do |line|
              formatted_text line, align: :center, size: 8, color: COLOR_SECONDARY
            end
          end
        end
      end
  end
end
