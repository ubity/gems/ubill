# frozen_string_literal: true


CONCERNS_PATH = File.join(__dir__, "ubill", "concerns", "**", "*.rb")
Dir.glob(CONCERNS_PATH) do |path|
  require_relative path
end

require_relative "hash"
require_relative "ubill/configuration"
require_relative "ubill/invoice"
require_relative "ubill/version"

module Ubill
  class Error < StandardError; end
  # Your code goes here...
end
