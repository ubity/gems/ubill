# Ubill

PDF Invoice generator.


## Installation

Install the gem and add to the application's Gemfile by executing:

```bash
$ bundle add ubill
```

If bundler is not being used to manage dependencies, install the gem by executing:

```bash
$ gem install ubill
```


## Configuration

Configuration must to be executed only once! Put it into a rails initializer if possible.

```ruby
# configure pdf invoice generation
Ubill.configure do
  page_size "A4"
  footer.configure do
    # add one or more lines to footer
    add_line do
      # add items to a line in the footer,
      # items will be separated by dots
      append "rechnung GmbH", styles: [:bold]
      append "Mattermoststraße 23"
      append "12345 Demoburg"
      append "info@example.org"
      append "www.example.org"
    end
    add_line do
      append "Geschäftsführer Fried Hof, Holger Manen, Manfred Erike"
    end
    add_line do
      append "HRB 4711"
      append "Amtsgericht Jurahausen"
      append "Steuernummer 123/456/78900"
    end
  end
  header.configure do
    # header's left should be set to company name
    # header's right should contain sender address
    left "rechnung GmbH"
    right do
        append "Mattermoststraße 23"
        append "12345 Demoburg"
        append "info@example.org"
    end
  end
  account.configure do
    # configure bank account to which payments
    # should be sent
    recipient "rechnung GmbH"
    iban "DE081547114200"
    bic "ACCOUNTXXX"
  end
end
```


## Usage

```ruby
# Instantiate a new Invoice, this will use the
# settings configured above.
doc = Ubill::Invoice.new do
  # subject of the invoice, most probably that's
  # the invoice number. an additional info can be
  # provided which will be displayed just below subject
  # in a smaller font size
  subject("Rechnung R1234567890", "Rechnungsdatum 01.01.2100")
  # recipient address
  recipient(
    organisation: "Spielvereinigung Demoburg e.V.",
    name: "Tobi Ornottobi",
    department: "Dept. Purchase",
    street_1: "Highwaytohell 2",
    street_2: "Room 3.12",
    zip: "12345",
    city: "Coruscant",
    country: "Galactic Empire"
  )
  # add positions to invoice. prices, units or any other
  # numbers should be provided as strings, as we do not
  # interpret numbers with locale settings. There is also
  # no summing up of positions to totals! you have to
  # calculate and provide totals all by yourselves!
  4.times do |idx|
    position(
        # name, context and description are rendered into
        # one cell in the table as consecutive lines
        name: "Standard Abonnement",
        description: "SpVgg Demoburg -- Platz #{idx+1}\nStandard Abonnement für ein Jahr",
        price_per_unit: "7,50 € / Monat",
        units: "12",
        price: "90,00 €"
    )
  end
  # you have to calculate and provide totals all by yourselves!
  total "180,00 €"
  due "24.12.2100"
end

# save document
doc.save_as("invoice.pdf")
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
