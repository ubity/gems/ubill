# frozen_string_literal: true

require_relative "lib/ubill/version"

Gem::Specification.new do |spec|
  spec.name = "ubill"
  spec.version = Ubill::VERSION
  spec.authors = ["couchbelag"]
  spec.email = ["ts@ubity.io"]

  spec.summary = "PDF invoice generator."
  spec.description = "PDF invoice generator by ubity UG (https://ubity.io)."
  spec.homepage = "https://gitlab.com/ubity/gems/ubill"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.1.3"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) || f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor])
    end
  end
  spec.bindir = "bin"
  spec.executables = spec.files.grep(%r{\Abin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "prawn", "~> 2.4"
  spec.add_dependency "prawn-table", "~> 0.2"
  # prawn relies on gem matrix, but an incompatibility with
  # ruby 3.1 requires to add this dependency manually
  # (https://github.com/prawnpdf/prawn/issues/1273)
  spec.add_dependency "matrix", "~> 0.4"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
